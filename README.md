# Spaaza Webhook TestApp

This is a minimal app to test Webhook integration. Exposes to endpoints:

  `GET /` Will render a list of all activity recorded.

   `POST /webhookhandler` Expects a JSON object with the webhook information. Will record that object and will be available in the activity endpoint.

## How to use

First you need to deploy this project in a node environment, could be a AWS Lambda function, use [heroku](https://www.heroku.com/) or [now](https://zeit.co/now).

### I prefer `now`

In the project folder install `now`:

  `npm install now`

Then deploy the app by simply typing:

  `now`

This will give you a public URL you can use to test your webhook.

### Run locally

To run locally:

  `npm start`
