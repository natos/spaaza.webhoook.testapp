var fs = require('fs')
var express = require('express')
var bodyParser = require('body-parser')
var app = express()
var activity = {"name":"WebhookTestApp","events":[]}

app.use(bodyParser.json());

app.post('/webhookhandler', function (req, res) {
  activity.events.push(req.body);
  console.log('pushing event:', req.body);
  res.send(req.body);
})

app.get('/', function(req, res) {
  var body = '<h1>Webhook activity</h1>';
  body += '<ol>'
  for (var i = 0; i < activity.events.length; i += 1) {
    var event = activity.events[i];
    body += "<li>"
    body += "<date>" + event.created + "</date>"
    body += "<h4>Event: " + event.type + "</h4>"
    for (var prop in event.data) {
      body += "<p><em>" + prop + "</em>: <strong>" + event.data[prop] + "</strong></p>"
    }
    body += "</li>"
  }

  body += '</ol>'

  res.send(body)
})


app.listen(3000, function () {
  console.log('WebhookTestApp listening on port 3000!')
})
